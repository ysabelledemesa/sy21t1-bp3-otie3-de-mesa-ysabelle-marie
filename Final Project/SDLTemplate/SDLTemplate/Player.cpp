#include "Player.h"

void Player::start()
{
	texture = loadTexture("gfx/blue.png");

	x = 100;
	y = 100;
	width = 0;
	height = 0;
	isAlive = true;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	addBody(x, y);
}

void Player::update()
{
	if (!isAlive) return;

	if (app.keyboard[SDL_SCANCODE_W])
	{
		body[0]->y -= 3;
	}

	if (app.keyboard[SDL_SCANCODE_S])
	{
		body[0]->y += 3;
	}

	if (app.keyboard[SDL_SCANCODE_A])
	{
		body[0]->x -= 3;
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
		body[0]->x += 3;
	}

	for (int i = body.size() - 1; i > 0; i--)
	{
		body[i]->x = body[i - 1]->x;
		body[i]->y = body[i - 1]->y;
	}

	for (int i = 0; i < body.size(); i++)
	{
		if (isAlive == false)
		{
			Body* bodyToErase = body[i];
			body.erase(body.begin() + i);
			delete bodyToErase;

			break;
		}
	}
}

void Player::draw()
{
	if (!isAlive) return;

	for (int i = 0; i < body.size(); i++)
	{
		blit(texture, body[i]->x, body[i]->y);
	}
}

int Player::getPositionX()
{
	return x;
}

int Player::getPositionY()
{
	return y;
}

int Player::getWidth()
{
	return width;
}

int Player::getHeight()
{
	return height;
}

bool Player::getIsAlive()
{
	return isAlive;
}

void Player::doDeath()
{
	isAlive = false;
}

void Player::addBody(int x, int y)
{
	Body* bodyPart = new Body(x, y);
	body.push_back(bodyPart);
}

bool Player::setIsAlive()
{
	isAlive = true;
	return isAlive;
}

std::vector<Body*> const& Player::getBody() const
{
	// TODO: insert return statement here
	return body;
}




