#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);

	points = 0;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();

	for (int i = 0; i < 1; i++)
	{
		spawn();
	}
}

void GameScene::draw()
{
	Scene::draw();

	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 3, 255, 255, 255, TEXT_CENTER, "GAME OVER!");

		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 255, 255, 255, TEXT_CENTER, "PLAY AGAIN? 1 FOR YES / 2 FOR NO");
	}
}

void GameScene::update()
{
	Scene::update();
	
	doSpawnLogic();
	doCollisionLogic();
	wallCollision();
	restartGame();
}

void GameScene::doCollisionLogic()
{
	Body* addedBody = player->getBody()[0];

	for (int i = 0; i < objects.size(); i++)
	{
		Fruit* currentFruit = dynamic_cast<Fruit*>(objects[i]);

		if (currentFruit != NULL)
		{
			int collision = checkCollision(
				addedBody->x, addedBody->y, player->getWidth(), player->getHeight(),
				currentFruit->getPositionX(), currentFruit->getPositionY(), currentFruit->getWidth(), currentFruit->getHeight()
			);

			if (collision == 1)
			{
				despawnFruit(currentFruit);
				points++;
				player->addBody(addedBody->x - 5, addedBody->y - 5);
				break;
			}
		}
	}
}

void GameScene::spawn()
{
	Fruit* fruit = new Fruit();
	this->addGameObject(fruit);

	fruit->setPosition(rand() % 780 + 20, rand() % 720 + 20);

	spawnedFruit.push_back(fruit);
}

void GameScene::doSpawnLogic()
{
	if (spawnedFruit.size() == 0)
	{
		spawn();
	}
}

void GameScene::despawnFruit(Fruit* fruit)
{
	int index = -1;
	for (int i = 0; i < spawnedFruit.size(); i++)
	{
		if (fruit == spawnedFruit[i])
		{
			index = i;
			break;
		}
	}

	if (index != -1)
	{
		spawnedFruit.erase(spawnedFruit.begin() + index);
		delete fruit;
	}
}

void GameScene::wallCollision()
{
	Body* body = player->getBody()[0];

	for (int i = 0; i < objects.size(); i++)
	{
		if (body->x < 0 || body->x > 800)
		{
			player->doDeath();
		}

		if (body->y < 0 || body->y > 800)
		{
			player->doDeath();
		}
	}
}

void GameScene::restartGame()
{
	if (player->getIsAlive() == false)
	{
		if (app.keyboard[SDL_SCANCODE_1])
		{
			points = 0;
			player->setIsAlive();
		}

		if (app.keyboard[SDL_SCANCODE_2])
		{
			drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 1.5, 255, 255, 255, TEXT_CENTER, "TRY AGAIN NEXT TIME!");
		}
	}
}
