#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include <vector>
#include "util.h"
#include "Fruit.h"

struct Body
{
    int x;
    int y;

    Body(int xPos, int yPos)
    {
        x = xPos;
        y = yPos;
    }
};
class Player :
    public GameObject
{
public:
    void start();
    void update();
    void draw();

    int getPositionX();
    int getPositionY();
    int getWidth();
    int getHeight();
    bool getIsAlive();
    void doDeath();
    void addBody(int x, int y);
    bool setIsAlive();
    std::vector<Body*> const &getBody() const;

private:
    SDL_Texture* texture;
    int x;
    int y;
    int width;
    int height;
    std::vector<Body*> body;
    bool isAlive;

};

