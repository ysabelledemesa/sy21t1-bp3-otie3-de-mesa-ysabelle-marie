#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);

	points = 0;
	powerUpCount = 0;
	killCount = 0;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();
	currentSpawnTimer = 300;
	spawnTime = 300; //spawn time of 5 seconds
	currentPowerUpSpawnTimer = 450;
	powerUpSpawnTime = 450;

	for (int i = 0; i < 3; i++) //spawning 3 enemies
	{
		spawn();
	}

	if (killCount >= 2)
	{
		for (int i = 0; i < 1; i++)
		{
			spawnBoss();
			killCount = 0;
		}
	}
}

void GameScene::draw()
{
	Scene::draw();

	//available in text.h
	//white text, only supports uppercase letters, special characters, and numbers
	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	//game over text
	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 255, 255, 255, TEXT_CENTER, "GAME OVER!");
	}
}

void GameScene::update()
{
	Scene::update();

	doSpawnLogic();
	doCollisionLogic();
	doPowerUpCollision();
}

void GameScene::doSpawnLogic()
{
	if (currentSpawnTimer > 0)
		currentSpawnTimer--;

	if (currentSpawnTimer <= 0)
	{
		for (int i = 0; i < 3; i++) //spawning 3 enemies
		{
			spawn();
		}

		currentSpawnTimer = spawnTime;
	}

	if (currentPowerUpSpawnTimer > 0)
		currentPowerUpSpawnTimer--;

	if (currentPowerUpSpawnTimer <= 0)
	{
		for (int i = 0; i < 1; i++)
		{
			spawnPowerUp();
		}

		currentPowerUpSpawnTimer = powerUpSpawnTime;
	}

	if (killCount >= 9)
	{
		for (int i = 0; i < 1; i++)
		{
			spawnBoss();
			killCount = 0;
		}
	}
}

void GameScene::doCollisionLogic()
{
	//for checking collision
	for (int i = 0; i < objects.size(); i++)
	{
		//cast to bullet or checking if object is a type of a class
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);

		//check if cast was successful by using a NULL check (checking if object is a bullet)
		if (bullet != NULL)
		{
			//check which side the bullet is on, player or enemy
			if (bullet->getSide() == Side::ENEMY_SIDE) //if the bullet is from the enemy side
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1) //checkCollision returns 1 if something is registered
				{
					player->doDeath();
					break;
				}
			}
			else if (bullet->getSide() == Side::PLAYER_SIDE) //if the bullet is from the player side
			{
				for (int i = 0; i < spawnedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spawnedEnemies[i];

					int collision = checkCollision(
						currentEnemy->getPositionX(), currentEnemy->getPositionY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);

					if (collision == 1)
					{
						despawnEnemy(currentEnemy);
						//increase points every time an enemy dies 
						points++;
						killCount++;
						//ONLY DESPAWN ONE AT A TIME else crash due to looping while deleting
						break;
					}
				}
			}
		}
	}
}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setPlayerTarget(player);

	enemy->setPosition(300 + (rand() % 300), -100);
	spawnedEnemies.push_back(enemy); //keeping track of spawned enemies
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		if (enemy == spawnedEnemies[i])
		{
			index = i;
			break;
		}
	}

	//if a match is found
	if (index != -1)
	{
		spawnedEnemies.erase(spawnedEnemies.begin() + index);
		delete enemy;
	}
}

void GameScene::spawnPowerUp()
{
	PowerUp* powerUp = new PowerUp();
	this->addGameObject(powerUp);
	powerUp->setPosition(100 + (rand() % 700 + 20), -100);
	spawnedPowerUps.push_back(powerUp);
}

void GameScene::despawnPowerUp(PowerUp* powerUp)
{
	int index = -1;
	for (int i = 0; i < spawnedPowerUps.size(); i++)
	{
		if (powerUp == spawnedPowerUps[i])
		{
			index = i;
			break;
		}
	}

	if (index != -1)
	{
		spawnedPowerUps.erase(spawnedPowerUps.begin() + index);
		delete powerUp;
	}
}

void GameScene::doPowerUpCollision()
{
	for (int i = 0; i < objects.size(); i++)
	{
		PowerUp* powerUp = dynamic_cast<PowerUp*>(objects[i]);

		if (powerUp != NULL)
		{
			for (int i = 0; i < spawnedPowerUps.size(); i++)
			{
				PowerUp* currentPowerUp = spawnedPowerUps[i];

				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					currentPowerUp->getPositionX(), currentPowerUp->getPositionY(), currentPowerUp->getWidth(), currentPowerUp->getHeight()
				);

				if (collision == 1)
				{
					powerUpCount++;

					for (int i = 0; i < powerUpCount; i++)
					{
						reloadTime = 8; //8 frames or every 0.13 seconds
						currentReloadTime = 0;

						if (currentReloadTime > 0)
							currentReloadTime--;

						if (app.keyboard[SDL_SCANCODE_F] && currentReloadTime == 0)
						{
							Bullet* bullet = new Bullet(player->getPositionX() - 2, player->getPositionY(), 0, -1, 10, Side::PLAYER_SIDE);
							powerUpBullets.push_back(bullet);
							addGameObject(bullet);

							currentReloadTime = reloadTime;
						}
					}
				}
				
				for (int i = 0; i < powerUpBullets.size(); i++)
				{
					if (powerUpBullets[i]->getPositionX() > SCREEN_WIDTH)
					{
						Bullet* bulletToErase = powerUpBullets[i];
						powerUpBullets.erase(powerUpBullets.begin() + i);
						delete bulletToErase;

						//only delete one bullet at a time or per frame
						break;
					}
				}

				if (collision == 1)
				{
					despawnPowerUp(currentPowerUp);

					break;
				}
			}
		}
	}
}

void GameScene::spawnBoss()
{
	currentSpawnTimer = 10000;
	spawnTime = 10000;
	Boss* boss = new Boss();
	this->addGameObject(boss);
	boss->setPlayerTarget(player);

	boss->setPosition(300 + (rand() % 300), -100);
	spawnedBoss.push_back(boss); //keeping track of spawned enemies
}

void GameScene::despawnBoss(Boss* boss)
{
	int index = -1;
	for (int i = 0; i < spawnedBoss.size(); i++)
	{
		if (boss == spawnedBoss[i])
		{
			index = i;
			break;
		}
	}

	//if a match is found
	if (index != -1)
	{
		spawnedBoss.erase(spawnedBoss.begin() + index);
		delete boss;
	}
}

void GameScene::bossFight()
{
	for (int i = 0; i < objects.size(); i++)
	{
		//cast to bullet or checking if object is a type of a class
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);

		//check if cast was successful by using a NULL check (checking if object is a bullet)
		if (bullet != NULL)
		{
			//check which side the bullet is on, player or enemy
			if (bullet->getSide() == Side::ENEMY_SIDE) //if the bullet is from the enemy side
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1) //checkCollision returns 1 if something is registered
				{
					player->doDeath();
					break;
				}
			}
			else if (bullet->getSide() == Side::PLAYER_SIDE) //if the bullet is from the player side
			{
				for (int i = 0; i < spawnedBoss.size(); i++)
				{
					Boss* currentBoss = spawnedBoss[i];

					int hp = 5;

					int collision = checkCollision(
						currentBoss->getPositionX(), currentBoss->getPositionY(), currentBoss->getWidth(), currentBoss->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);

					if (hp <= 0)
					{
						despawnBoss(currentBoss);
						//increase points every time an enemy dies 
						points++;
						currentSpawnTimer = 300;
						spawnTime = 300;
						//ONLY DESPAWN ONE AT A TIME else crash due to looping while deleting
						break;
					}

					if (collision == 1)
					{
						hp--;
						break;
					}
				}
			}
		}

	}
}



