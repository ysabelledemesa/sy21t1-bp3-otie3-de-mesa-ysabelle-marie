#include "PowerUp.h"

PowerUp::PowerUp()
{
}

PowerUp::~PowerUp()
{
}

void PowerUp::start()
{
	//loading texture
	texture = loadTexture("gfx/button.png");

	//initialize to avoid garbage variables
	directionX = 0;
	directionY = 1;
	width = 0;
	height = 0;
	speed = 2;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/342749__rhodesmas__notification-01.ogg");
	sound->volume = 64; //128 max volume
}

void PowerUp::update()
{
	x += directionX * speed;
	y += directionY * speed;

	for (int i = 0; i < powerUp.size(); i++)
	{
		if (powerUp[i]->getPositionY() > SCREEN_HEIGHT)
		{
			PowerUp* powerUpToErase = powerUp[i];
			powerUp.erase(powerUp.begin() + i);
			delete powerUpToErase;

			break;
		}
	}
}

void PowerUp::draw()
{
	blit(texture, x, y);
}

void PowerUp::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int PowerUp::getPositionX()
{
	return x;
}

int PowerUp::getPositionY()
{
	return y;
}

int PowerUp::getWidth()
{
	return width;
}

int PowerUp::getHeight()
{
	return height;
}
