#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include <vector>
#include "text.h"
#include "Fruit.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
private:
	Player* player;

	std::vector<Fruit*> spawnedFruit;

	void doCollisionLogic();
	void spawn();
	void doSpawnLogic();
	void despawnFruit(Fruit* fruit);
	void wallCollision();
	void restartGame();

	int points;
};

