#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include <vector>
#include "Player.h"
#include "util.h"

class Fruit :
    public GameObject
{
public:
    Fruit();
    ~Fruit();
    void start();
    void update();
    void draw();
    void setPosition(int xPos, int yPos);

    int getPositionX();
    int getPositionY();
    int getWidth();
    int getHeight();

private:
    SDL_Texture* texture;
    int x;
    int y;
    int width;
    int height;
};

