#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include <vector>
#include "text.h"
#include "PowerUp.h"
#include "Bullet.h"
#include "Boss.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
private:
	Player* player;

	float spawnTime;
	float currentSpawnTimer;
	float powerUpSpawnTime;
	float currentPowerUpSpawnTimer;
	float bossSpawnTime;
	float currentBossSpawnTimer;
	std::vector<Enemy*> spawnedEnemies;
	std::vector<PowerUp*> spawnedPowerUps;
	std::vector<Boss*> spawnedBoss;

	void doSpawnLogic();
	void doCollisionLogic();
	void spawn();
	void despawnEnemy(Enemy* enemy);
	void spawnPowerUp();
	void despawnPowerUp(PowerUp* powerUp);
	void doPowerUpCollision();
	void spawnBoss();
	void despawnBoss(Boss* boss);
	void bossFight();


	int points;
	int powerUpCount;
	int killCount;
	float reloadTime;
	float currentReloadTime;
	std::vector<Bullet*> powerUpBullets;
};

