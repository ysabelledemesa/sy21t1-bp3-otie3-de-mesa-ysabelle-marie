#include "Fruit.h"

Fruit::Fruit()
{
}

Fruit::~Fruit()
{
}

void Fruit::start()
{
	texture = loadTexture("gfx/red.png");

	width = 0;
	height = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Fruit::update()
{
}

void Fruit::draw()
{
	blit(texture, x, y);
}

void Fruit::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int Fruit::getPositionX()
{
	return x;
}

int Fruit::getPositionY()
{
	return y;
}

int Fruit::getWidth()
{
	return width;
}

int Fruit::getHeight()
{
	return height;
}

